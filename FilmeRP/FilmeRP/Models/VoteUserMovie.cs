﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FilmeRP.Models
{
    public class VoteUserMovie
    {        
        public int UserId { get; set; }
        public User User { get; set; }
        public int MovieId { get; set; }        
        public Movie Movie { get; set; }
        public int Vote { get; set; }
    }

}
