﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FilmeRP.Models
{
    public class Movie
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(5000)")]
        public string Description { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string Genre { get; set; }
        public List<Actor> Actors { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string Director { get; set; }
        public int Year { get; set; }

        public virtual ICollection<VoteUserMovie> VoteUserMovies { get; set; }


    }
}
