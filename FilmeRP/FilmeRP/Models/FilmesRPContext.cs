﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmeRP.Models
{
    public class FilmesRPContext : DbContext
    {
        public FilmesRPContext()
        { }
        public FilmesRPContext(DbContextOptions<FilmesRPContext> options)
        : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=Filme_Parenza;Data Source=RAFAEL\PARENZA");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VoteUserMovie>(entity=>
            {
                entity.HasKey(e => new { e.MovieId, e.UserId });
            });
        }
        public DbSet<User> User { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Actor> Actor { get; set; }
        public DbSet<Actor> VoteUserMovie { get; set; }
    }
}
