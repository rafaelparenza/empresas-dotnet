﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmeRP.Models;
using Microsoft.AspNetCore.Authorization;

namespace FilmeRP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdministradorController : ControllerBase
    {
        private readonly FilmesRPContext _context;

        public AdministradorController(FilmesRPContext context)
        {
            _context = context;
        }

        // GET: api/Administrador
        [HttpGet("{numeroPorPagina}")]
        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUser(int numeroPorPagina)
        {
            if (numeroPorPagina > 0)
                return await _context.User.OrderBy(x => x.Username).Take(numeroPorPagina).ToListAsync();
            else
                return await _context.User.OrderBy(x => x.Username).ToListAsync();
        }

        // PUT: api/Administrador/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<IActionResult> PutUser(int id, ModelUser userModel)
        {
            var user = new User()
            {
                Password = userModel.Password,
                Username = userModel.Username,
                Active = userModel.Active,
            };
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Administrador
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Route("Create")]
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public async Task<ActionResult<ModelUser>> PostUser(ModelUser userModel)
        {
            var user = new User()
            {
                Password = userModel.Password,
                Username = userModel.Username,
                Active = userModel.Active,
                Role = "Administrador"
            };

            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // DELETE: api/Administrador/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            user.Active = false;
            await _context.SaveChangesAsync();
            // _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Id == id);
        }
    }

    public class ModelUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
    }


}
