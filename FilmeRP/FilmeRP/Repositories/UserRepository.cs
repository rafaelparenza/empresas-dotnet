﻿using FilmeRP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmeRP.Repositories
{

    public class UserRepository
    {
        private readonly FilmesRPContext _context;
        public UserRepository(FilmesRPContext context){
            _context = context;
        }        
        public User Get(string username, string password) =>      
            _context.User.FirstOrDefault(x => x.Username == username && x.Password == password && x.Active==true);
    }
}
